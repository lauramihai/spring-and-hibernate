package com.example.demo;

public class BaseballCoach implements Coach {
    //define a private field for the dependency 
    private FortuneService fortuneService;
    //define constructor for dependency injection
    public BaseballCoach(FortuneService theFortuneService){
        fortuneService = theFortuneService;
    }
    @Override
    public String getWorkout(){
        return "Working as baseball coach";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }
}
