package com.example.demo;

public interface Coach {
    public String getWorkout();
    public String getDailyFortune();
}
