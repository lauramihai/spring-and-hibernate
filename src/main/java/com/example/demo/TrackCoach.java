package com.example.demo;

public class TrackCoach implements Coach {
    private FortuneService fortuneService;

    public TrackCoach(FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    @Override
    public String getWorkout(){
        return "Working as track coach";
    }

    @Override
    public String getDailyFortune() {
        return "Running..." + fortuneService.getFortune();
    }
}
